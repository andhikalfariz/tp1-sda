import java.io.*;
import java.util.*;
import java.lang.Object.*;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.LinkedTransferQueue;
import java.lang.Math.*;

/**
 * Kolaborator : Gani Ilham Irsyadi (Ide dan membantu debugging) Muhammad Alif
 * Saddid (Ide dan membantu debugging) Ibnu Hambali (Ide dan membantu debugging)
 */

public class LadangBoba {
    private static InputReader in = new InputReader(System.in);
    private static PrintWriter out = new PrintWriter(System.out);
    private static ArrayList<Integer> bobaTiapLadang = new ArrayList<Integer>();
    private static Queue<Orang> queueOrang = new LinkedList<Orang>();
    private static Queue<Orang> queueIzuri = new LinkedList<Orang>();
    private static ArrayList<Keranjang> listKeranjang = new ArrayList<Keranjang>();
    private static HashMap<String, Keranjang> hashKeranjang = new HashMap<String, Keranjang>();

    public static void main(String[] args) {
        int jumlahLadang; // N
        int banyakBoba; // Ai, banyak boba yg dihasilkan ladang ke i
        int tempBoba; // untuk menghitung boba sementara
        int jumlahKeranjang; // M
        String namaKeranjang; // S
        int kapasitasAwal; // C
        int fleksibilitas; // F
        int lamaHari; // H
        String query; // query
        int jumlahAntrian; // Y
        String namaPembeli; // R
        int batasPermintaan; // O
        String namaTarget; // Untuk keranjang yang diberi perlakuan oleh Orang
        int newC;
        int newF;

        // Jumlah ladang beserta banyaknya boba yang dihasilkan masing-masing ladang
        jumlahLadang = in.nextInt();
        for (int i = 0; i < jumlahLadang; i++) {
            banyakBoba = in.nextInt();
            bobaTiapLadang.add(banyakBoba);
        }

        // Jumlah keranjang berserta atributnya
        jumlahKeranjang = in.nextInt();
        for (int i = 0; i < jumlahKeranjang; i++) {
            namaKeranjang = in.next();
            kapasitasAwal = in.nextInt();
            fleksibilitas = in.nextInt();

            Keranjang keranjang = new Keranjang(namaKeranjang, kapasitasAwal, fleksibilitas);
            listKeranjang.add(keranjang);
            hashKeranjang.put(keranjang.getNama(), keranjang);
        }

        // Lamanya hari ladang tersebut bisa dipanen
        lamaHari = in.nextInt();

        out.println("Hari ke-" + 1 + ':');
        out.println("Hasil Panen");
        for (Keranjang k : listKeranjang) {
            k.cariMaxBoba(bobaTiapLadang);
            out.println(k.getNama() + " " + k.getMaxBoba());
        }




        for (int i = 0; i < lamaHari - 1; i++) {
            // Query untuk Izuri
            query = in.next();
            namaTarget = in.next();
            Orang izuri = new Orang("IZURI", query, namaTarget);
            if (query.equalsIgnoreCase("ADD") || query.equalsIgnoreCase("UPDATE")) {
                newC = in.nextInt();
                newF = in.nextInt();
                izuri.setNewC(newC);
                izuri.setNewF(newF);

            } else if (query.equalsIgnoreCase("RENAME")) {
                String newNama = in.next();
                izuri.setNewNama(newNama);

            }

            queueIzuri.add(izuri);

            // Query untuk orang-orang lain
            jumlahAntrian = in.nextInt();

            for (int ii = 0; ii < jumlahAntrian; ii++) {
                namaPembeli = in.next();
                query = in.next();
                namaTarget = in.next();

                Orang orang = new Orang(namaPembeli, query, namaTarget);
                if (query.equalsIgnoreCase("ADD") || query.equalsIgnoreCase("UPDATE")) {
                    newC = in.nextInt();
                    newF = in.nextInt();
                    orang.setNewC(newC);
                    orang.setNewF(newF);

                } else if (query.equalsIgnoreCase("RENAME")) {
                    String newNama = in.next();
                    orang.setNewNama(newNama);

                }
                queueOrang.add(orang);
            }

            // menjalankan query Izuri
            Orang Izuri = queueIzuri.poll();
            String queryIzuri = Izuri.getQuery();
            if (queryIzuri.equalsIgnoreCase("add")) {
                if (hashKeranjang.get(Izuri.getNamaTarget()) == null) {
                    addKeranjang(Izuri, listKeranjang);
                }

            } else if (queryIzuri.equalsIgnoreCase("update")) {
                if (hashKeranjang.get(Izuri.getNamaTarget()) != null) {
                    updateKeranjang(Izuri, listKeranjang);
                }

            } else if (queryIzuri.equalsIgnoreCase("sell")) {
                if (hashKeranjang.get(Izuri.getNamaTarget()) != null) {
                    sellKeranjang(Izuri, listKeranjang);
                }

            } else if (queryIzuri.equalsIgnoreCase("rename")) {
                if (hashKeranjang.get(Izuri.getNamaTarget()) != null) {
                    renameKeranjang(Izuri, listKeranjang);
                }

                // Menjalankan query orang selain Izuri
                batasPermintaan = in.nextInt();

                for (int j = 0; j < batasPermintaan; j++) {
                    Orang org = queueOrang.poll();
                    String queryOrang = org.getQuery();
                    if (queryOrang.equalsIgnoreCase("add")) {
                        if (hashKeranjang.get(org.getNamaTarget()) == null) {
                            addKeranjang(org, listKeranjang);
                        }

                    } else if (queryOrang.equalsIgnoreCase("update")) {
                        if (hashKeranjang.get(org.getNamaTarget()) != null) {
                            updateKeranjang(org, listKeranjang);
                        }

                    } else if (queryOrang.equalsIgnoreCase("sell")) {
                        if (hashKeranjang.get(org.getNamaTarget()) != null) {
                            sellKeranjang(org, listKeranjang);
                        }

                    } else if (queryOrang.equalsIgnoreCase("rename")) {
                        if (hashKeranjang.get(org.getNamaTarget()) != null) {
                            renameKeranjang(org, listKeranjang);
                        }
                    }
                }

                out.println("Hari ke-" + i);

                out.println("Permintaan yang dilayani");
                for (int k = 0; k < batasPermintaan; k++) {
                    out.print(queueOrang.poll().getNama() + " ");
                }

                out.println("Hasil Panen");
                for (Keranjang k : listKeranjang) {
                    k.cariMaxBoba(bobaTiapLadang);
                    out.println(k.getNama() + " " + k.getMaxBoba());
                }
            }
        }
        out.close();
    }

    // Method untuk addKeranjang
    public static void addKeranjang(Orang orang, ArrayList<Keranjang> listKeranjang) {
        Keranjang k = new Keranjang(orang.getNamaTarget(), orang.getNewC(), orang.getNewF());
        hashKeranjang.put(k.getNama(), k);
        listKeranjang.add(k);
    }

    // Method untuk updateKeranjang BELOM KELAR
    public static void updateKeranjang(Orang orang, ArrayList<Keranjang> listKeranjang) {
        Keranjang k = getByNama(orang.getNamaTarget());
        k.setKapasitas(orang.getNewC());
        k.setFleksibilitas(orang.getNewF());
    }

    // Method untuk sellKeranjang
    public static void sellKeranjang(Orang orang, ArrayList<Keranjang> listKeranjang) {
        Keranjang k = getByNama(orang.getNamaTarget());
        hashKeranjang.remove(k.getNama());
        listKeranjang.remove(k);
    }

    // Method untuk renameKeranjang
    public static void renameKeranjang(Orang orang, ArrayList<Keranjang> listKeranjang) {
        Keranjang k = getByNama(orang.getNamaTarget());
        hashKeranjang.remove(k.getNama());
        k.setNama(orang.getNamaTarget());
        hashKeranjang.put(k.getNama(), k);
    }

    void sort(int a[]) throws Exception {
        for (int i = a.length - 1; i >= 0; i--) {
            boolean swapped = false;
            for (int j = 0; j < i; j++) {
                if (a[j] > a[j + 1]) {
                    int T = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = T;
                    swapped = true;
                }
            }
            if (!swapped)
                return;
        }
    }

    public static Keranjang getByNama(String nama) {
        for (Keranjang k : listKeranjang) {
            if (k.getNama() == nama) {
                return k;
            }
        }
        return null;
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

    }

}

class Keranjang {
    private String nama;
    private int kapasitasAwal;
    private int fleksibilitas;
    private int isi;
    private int maxBoba;
    private int banyakBoba;
    private static int[][] arr = new int[105][105];

    public Keranjang(String nama, int kapasitasAwal, int fleksibilitas) {
        this.nama = nama;
        this.kapasitasAwal = kapasitasAwal;
        this.fleksibilitas = fleksibilitas;
    }

    public String getNama() {
        return this.nama;
    }

    public int getKapasitas() {
        return this.kapasitasAwal;
    }

    public int getFleksibilitas() {
        return this.fleksibilitas;
    }

    public void setKapasitas(int newKapasitas) {
        this.kapasitasAwal = newKapasitas;
    }

    public void setFleksibilitas(int newFleksibilitas) {
        this.fleksibilitas = newFleksibilitas;
    }

    public void setNama(String newNama) {
        this.nama = newNama;
    }

    public int getMaxBoba() {
        return this.maxBoba;
    }

    public void setIsi(int isi) {
        this.isi = isi;
    }

    public void cariMaxBoba(ArrayList<Integer> bobaTiapLadang) {
        /**
         * Menghitung hasil panen
         * l = posisi ladnag
         * p = upgrade ke p kali
         * arr[l][p] = banyak boba yang dapat diambil pada ladang ke-1 hingga ke-1 setelah upgrade p kali
         */
        // Menghitung hasil panen

        int jumlahLadang = bobaTiapLadang.size();

        for (int l=0; l<jumlahLadang; l++) {
            banyakBoba = bobaTiapLadang.get(l);

            if (l==0) {
                arr[0][0] = Math.min(this.kapasitasAwal, banyakBoba);
                arr[0][1] = 0;
            
            } else {
                for (int p=0; p<=l+1; p++) { //kata alip sadit
                    if(p==0){
                        arr[l][p] = arr[l-1][0] + Math.min(this.kapasitasAwal - arr[l-1][0], banyakBoba);
                    
                    } else if (p>0 && p<l) {
                        int a = arr[l-1][p] + Math.min(this.kapasitasAwal + p*this.fleksibilitas, banyakBoba);
                        int b = arr[l-1][p-1];
                        arr[l][p] = Math.max(a,b);
        
                    } else if (p+1 ==l) { //kata alipsadit
                        arr[l][p] = 0;
                    }
                }  
            }
        }

        
        // for (int i=0; i<jumlahLadang; i++) {
        //     for (int j=0; j<i; j++) {
        //         if (arr[i][j] > tempMax) {
        //             tempMax = arr[i][j];
        //         }
        //     }
        // }
        // this.maxBoba = tempMax; 

        int tempMax = arr[0][0];
        for (int i = 0; i <= jumlahLadang; i++){
            if (arr[jumlahLadang-1][i] > tempMax){
                tempMax = arr[jumlahLadang-1][i];
            }
        }
        this.maxBoba = tempMax;
    }
}

class Orang {
    private String namaOrang;
    private String query;
    private String namaTarget;
    private int newC;
    private int newF;
    private String newNama;

    public Orang(String namaOrang, String query, String namaTarget) {
        this.namaOrang = namaOrang;
        this.query = query;
        this.namaTarget = namaTarget;
    }

    public String getNama() {
        return this.namaOrang;
    }

    public String getQuery() {
        return this.query;
    }

    public String getNamaTarget() {
        return this.namaTarget;
    }

    public int getNewC() {
        return this.newC;
    }

    public int getNewF() {
        return this.newF;
    }

    public void setNewC(int newC) {
        this.newC = newC;
    }

    public void setNewF(int newF) {
        this.newF = newF;
    }

    public void setNewNama(String newNama) {
        this.newNama = newNama;
    }

}
